<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Planning</title>
		<link rel="stylesheet" href="MiseEnPage.css">
	</head>
	
	<body>
	
		<a href="Deconnexion.php" title="Deconnexion" >Deconnexion</a>
		<form id="formActivite" name="Activite" method="POST" action="Planning.php">
			<p> Activité: 
				<select id="ListeActivites" name="ListeActivites"> 
					<option value="Java">Java</option>
					<option value="Python">Python</option>
					<option value="Anglais">Anglais</option>	
					<option value="Repos">Repos</option>
					<option value="Café">Café</option>
					<option value="PHP">PHP</option>

				</select>
			</p>

			<p> Date:
				<select id="ListeJours" name="ListeJours">
					<?php
						session_start();
						for($x=1;$x<=9;$x++)
						{
							echo"<option value=0".$x.">0".$x." </option>";
						}
						for($x=10;$x<=31;$x++)
						{
							echo"<option value=".$x.">".$x." </option>";
						}
					?>
				</select>
				
				<select id="ListeMois" name="ListeMois">
					<?php
						for($x=1;$x<=9;$x++)
						{
							echo"<option value=0".$x.">0".$x." </option>";
						}
						for($x=10;$x<=12;$x++)
						{
							echo"<option value=".$x.">".$x." </option>";
						}
					?>
				</select>
				
				<select id="ListeAnnees" name="ListeAnnees"> 
					<option value=2014>2014</option>
					<option value=2015>2015</option>
					<option value=2016>2016</option>	
					<option value=2017>2017</option>
					<option value=2018>2018</option>
					<option value=2019>2019</option>
				</select>
			</p>
				
			<p> Heure: 
				<select id="ListeHeures" name="ListeHeures" >
					<?php
						for($x=8;$x<=20;$x++)
						{
							echo"<option value=".$x.">".$x." h</option>";
						}
					?>

				</select>
			</p>
			
			<p> <input type="submit" value="Valider"> </p>
		</form>
		


	
		<?php 

		date_default_timezone_set('Europe/Paris');
		
		$file_db=new PDO('sqlite:EmploisTemps.sqlite3');
		$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
		
		$date=date("d-m-Y");

		

		
		if(isset($_POST['ListeMoisS']))	{
			$j=$_POST['ListeJoursS'];
			$m=$_POST['ListeMoisS'];
			$a=$_POST['ListeAnneesS'];
			

			
			$_SESSION['date_du_jour']=$j."-".$m."-".$a;

			
			
			
			}


		$user=$_SESSION['id'];
		
		if(isset($_POST['ListeHeures']))
		{
			$j=$_POST['ListeJours'];
			$m=$_POST['ListeMois'];
			$a=$_POST['ListeAnnees'];
			
			$date=$j."-".$m."-".$a;

			
			$existe=$file_db->query("select count(*) as k from Activite where jour='".$date."' and idU='".$user."' and heure=".$_POST['ListeHeures']);
			$count=$existe->fetch();
			if($count['k']==0)
			{
				$insert="insert into Activite(nom,jour,heure,idU)VALUES(:nom,:jour,:heure,:idU)";
				$stmt=$file_db->prepare($insert);
				
				$stmt->bindParam(':nom',$_POST['ListeActivites']);
				$stmt->bindParam(':jour',$date);
				$stmt->bindParam(':heure',$_POST['ListeHeures']);
				$stmt->bindParam(':idU',$user);
				$stmt->execute();
			}
			else
			{
				$file_db->exec("update Activite set nom='".$_POST['ListeActivites']."' where jour='".$date."' and idU='".$user."' and heure=".$_POST['ListeHeures']);
			}
		}
		?>
		<?php 	echo '<p id="date">'.$_SESSION['date_du_jour'].'<p>'; ?>
		<table id="EmploiDuTemps">
			<tr>
				<th>Heure</th>
				<th>Activite</th>
			</tr>
			
		<?php
	
		
			for($x=8;$x<=20;$x++)
			{
				echo"<tr>";
					echo"<td>".$x."</td>";
					echo"<td>";
						$activites=$file_db->query("select nom, heure from Activite where jour='".$_SESSION['date_du_jour']."' and idU='".$user."'");
						foreach($activites as $a)
						{
							if($a['heure']==$x)
								echo $a['nom'];
						}
					echo"</td>";
				echo"</tr>";
			}
		?>
		</table>
		
		<form id="formDate" name="Date" method="POST" action="Planning.php">
			<p> Date:
				<select id="ListeJoursS" name="ListeJoursS">
					<?php
						for($x=1;$x<=9;$x++)
						{
							echo"<option value=0".$x.">0".$x." </option>";
						}
						for($x=10;$x<=31;$x++)
						{
							echo"<option value=".$x.">".$x." </option>";
						}
					?>
				</select>
				
				<select id="ListeMoisS" name="ListeMoisS">
					<?php
						for($x=1;$x<=9;$x++)
						{
							echo"<option value=0".$x.">0".$x." </option>";
						}
						for($x=10;$x<=12;$x++)
						{
							echo"<option value=".$x.">".$x." </option>";
						}
						?>
				</select>
				
				<select id="ListeAnneesS" name="ListeAnneesS"> 
					<option value=2014>2014</option>
					<option value=2015>2015</option>
					<option value=2016>2016</option>	
					<option value=2017>2017</option>
					<option value=2018>2018</option>
					<option value=2019>2019</option>
				</select>
				<input type="submit" value="Valider">
			</p>
		</form>
	</body>
</html>
