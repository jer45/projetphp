<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Inscription</title>
		<link rel="stylesheet" href="MiseEnPage.css"/>
	</head>
	
	<body>
		<a href="Deconnexion.php">Retour a l'ecran de connexion</a>
		<form id="formulaire_page_inscription" method="POST" action="inscription.php">
			<p>Identifiant : <input class="input" type="text" name="id" placeholder="Identifiant"></p>
			<p>Mot de passe : <input class="input" type="password" name="mdp" placeholder="Mot de passe"></p>
			<p>Confirmation du mot de passe : <input class="input" type="password" name="mdp_conf" placeholder="Mot de passe"></p>
			<p><input id="validation_inscription" type="submit" value="Inscription"></p>
		</form>
		
		<?php
			if(isset($_POST['id']))
			{	
				$succes=false;
				if($_POST['id']!="" and $_POST['mdp']==$_POST['mdp_conf'])
				{
					$file_db=new PDO('sqlite:EmploisTemps.sqlite3');
					$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
					
					$stmt=$file_db->query("Select count(*) as k from User where id='".$_POST['id']."'");
					$count=$stmt->fetch();
					
					if($count['k']>0)
						echo "Cet identifiant est deja utilise.<br>";
					else
					{
						$user=array
						(
							'id'=>$_POST['id'],
							'mdp'=>$_POST['mdp']
						);
						
						$insert="insert into User(id,mdp)values(:id,:mdp)";
						$stmt=$file_db->prepare($insert);
						
						$stmt->bindParam(':id',$id);
						$stmt->bindParam(':mdp',$mdp);
						
						$id=$user['id'];
						$mdp=$user['mdp'];
						$stmt->execute();
						
						$succes=true;
					}
				}
				else
				{
					if($_POST['id']=="")
						echo "- Identifiant invalide.<br>";
						
					if($_POST['mdp']!=$_POST['mdp_conf'])
						echo "- Les mots de passe ne correspondent pas.<br>";
				}
				if($succes)
					echo "<br>Inscription validee.<br>";
				else
					echo "<br>Echec de l'inscription.<br>";
			}
		?>
		
	</body>
</html>
