<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>creationBD</title>
	</head>
	<body>

		<?php 
		date_default_timezone_set('Europe/Paris');
		
		try
		{
			$file_db=new PDO('sqlite:EmploisTemps.sqlite3');
			$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
			
			// Suppression des tables existantes
			$file_db->exec("drop table User");
			$file_db->exec("drop table Activite");			
			
			// Creation des tables
			$file_db->exec("create table if not exists User(
			id text primary key,
			mdp text)");
			
			$file_db->exec("create table if not exists Activite(
			nom text,
			jour text,
			heure integer,
			idU text)");
			
			$User=array(
				array('id'=>'prof0',
				'mdp'=>'0'),
				array('id'=>'prof1',
				'mdp'=>'1'),
				array('id'=>'prof2',
				'mdp'=>'2'),
				array('id'=>'prof3',
				'mdp'=>'3'),
				array('id'=>'prof4',
				'mdp'=>'4'),
				array('id'=>'prof5',
				'mdp'=>'5'),
				array('id'=>'prof6',
				'mdp'=>'6'),
				array('id'=>'prof7',
				'mdp'=>'7'),
				array('id'=>'prof8',
				'mdp'=>'8'),
				array('id'=>'prof9',
				'mdp'=>'9'),
			);		
		
			$insert="insert into User(id,mdp)values(:id,:mdp)";
			$stmt=$file_db->prepare($insert);
			
			$stmt->bindParam(':id',$id);
			$stmt->bindParam(':mdp',$mdp);
			
			foreach ($User as $u){
				$id=$u['id'];
				$mdp=$u['mdp'];
				$stmt->execute();
			}
		}
	
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		?>
	</body>
</html>
