<!DOCTYPE html>
<html >
	<head>
		<meta charset="utf-8"/>
		<title>Authentification</title>
		<link rel="stylesheet" href="MiseEnPage.css"/>
	</head>
	
	<body>
		<form id="formulaire_connexion" method="POST">
			<p>Identifiant : <input class="input" type="text" name="id" placeholder="Identifiant"></p>
			<p>Mot de passe : <input class="input" type="password" name="mdp" placeholder="Mot de passe"></p>
			<p><input id="connexion" type="submit" value="Connexion"></p>
		</form>
		
		<form id="formulaire_inscription" method="POST" action="inscription.php">
			<p><input id="inscription" type="submit" value="Inscription"></p>
		</form>
		
		<?php
			session_start();
			date_default_timezone_set('Europe/Paris');
			if(isset($_POST['id']))
			{
				try
				{
					$file_db=new PDO('sqlite:EmploisTemps.sqlite3');
					$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
					
					$select="Select count(*) as k from User where id=:idE and mdp=:mdpE";
					$stmt=$file_db->prepare($select);
					
					$stmt->bindParam(':idE',$_POST['id']);
					$stmt->bindParam(':mdpE',$_POST['mdp']);
					$stmt->execute();
					$count=$stmt->fetch();
					
					if($count['k']==0)
					{
						echo "L'identifiant ou le mot de passe entré est incorrect.";
					}
					else
					{
						$_SESSION['date_du_jour']=date("d-m-Y");
						$_SESSION['id'] = $_POST['id'];
						header('Location:Planning.php');
					}
					
					//$file_db=null;
				}
				catch(PDOException $ex)
				{
					echo $ex->getMessage();
				}
			}
		?>
		
	</body>
</html>
